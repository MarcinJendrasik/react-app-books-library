import React, { Component } from "react";
import "../styles/AddBook.css";
import BookItem from "./BookItem";

class AddBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      minDate: new Date().toISOString().slice(0, 10),
      activeElement: 1
    };
  }

  handleCheckbox = e => {
    this.setState({
      checked: e.target.checked
    });
  };

  handleSelect = e => {
    this.setState({
      activeElement: +e.target.value
    });
  };

  handleClick = () => {
    console.log("Dodaj");
    this.props.add();
  };

  handleAddedBook = e => {
    e.preventDefault();
    this.props.activeBook(this.state.activeElement);
    this.props.activeBooks();
  };

  handleDate = e => {
    this.setState({
      date: e.target.value
    });
  };

  render() {
    let maxDate = this.state.minDate.slice(0, 4) * 1 + 1;
    maxDate = maxDate + "-12-31"; //2020-12-31

    const filterDropdown = this.props.users.filter((user) => {
      return user.id === this.state.activeElement;
    });

    return (
      <nav className="menu">
        <form>
          <label>Wybierz książki (max 3): </label>

          <select value={this.state.activeElement} onChange={this.handleSelect}>
            {this.props.users.map(user => (
              <option value={user.id}>
                {user.title}
              </option>
            ))}
          </select>
          <button className="btn" onClick={this.handleAddedBook}>dodaj</button>
          <div>
            {filterDropdown.map(user => (
              <div key={user.id}>
                <BookItem
                  key={user.id}
                  user={user}
                  change={this.changeBookStatus}
                  delete={this.deleteBook}
                />

                <br />
              </div>
            ))}
          </div>

          <input
            type="checkbox"
            checked={this.state.checked}
            id="important"
            onChange={this.handleCheckbox}
          />
          <label htmlFor="important">Priorytet</label>
          <br />
          <br />
          <label htmlFor="date"> Do kiedy oddać książkę: </label>
          <input
            type="date"
            value={this.state.date}
            min={this.state.minDate}
            max={maxDate}
            onChange={this.handleDate}
          />
          <br />
          <hr className="new4" />
          <div className="centerize">
            <button onClick={this.handleClick}>DODAJ</button>
          </div>
        </form>
      </nav>
    );
  }
}

export default AddBook;
