import React, { Component } from "react";
import "../styles/App.css";
import BookItem from "./BookItem";
import Header from "./Header";
import SectionsBottom from "./SectionsBottom";
import AddBook from "./AddBook";
import SectionBooks from "./SectionBooks";

class App extends Component {
  state = {
    users: [],
    select: "normal",
    value: "",
    minDate: new Date().toISOString().slice(0, 10)
  };

  componentDidMount() {
    fetch("data/jsonTab.json")
      .then(response => response.json())
      .then(data => {
        this.setState({
          users: data.users
        });
      });
  }

  activeBook = id => {
    this.state.users.forEach((user, i) =>{
      if (user.id === id) {
        user.active = true;
      }
    });

    this.setState({
      users: this.state.users
    })
  };

  activeBooks = () => {
    let activeUsers = this.state.users
                          .filter(user => user.active)
                          .map(user => (
      <BookItem
        key={user.id}
        user={user}
        change={this.changeBookStatus}
        delete={this.deleteBook}
      />
    ));
    return (
      <>
        {activeUsers.length > 0 ? (
          activeUsers
        ) : (
          <p>BRAK WYPOŻYCZONYCH KSIĄŻEK</p>
        )}
      </>
    );
  };

  doneBooks = () => {
    let doneUsers = this.state.users
                        .filter(user => !user.active)
                        .map(user => (
      <BookItem
        key={user.id}
        user={user}
        change={this.changeBookStatus}
        delete={this.deleteBook}
      />
    ));
    return <>{doneUsers.length > 0 ? doneUsers : <p></p>}</>;
  };

  changeBookStatus = id => {
    console.log("change elementu o id" + id);
    const users = Array.from(this.state.users);
    users.forEach(user => {
      if (user.id === id) {
        user.active = false;
        user.finishDate = new Date().getTime();
      }
    });
    this.setState({
      users: users
    });
  };

  deleteBook = id => {
    console.log("delete elementu o id" + id);
    const users = [...this.state.users];
    console.log(users);
    const index = users.findIndex(user => user.id === id);
    users.splice(index, 1);
    console.log(users);

    this.setState({
      users
    });
  };

  addTask = () => {
    console.log("dodany obiekt");
    return true;
  };

  render() {
    let maxDate = this.state.minDate.slice(0, 4) * 1 + 1;
    console.log(maxDate);
    maxDate = maxDate + "-12-31";

    return (
      <>
        <div className="grid">
          <Header />
          <SectionsBottom />
          <AddBook
              add={this.addTask}
              users={this.state.users}
              activeBook={this.activeBook}
              activeBooks={this.activeBooks}
          />
          <SectionBooks
            activeBooks={this.activeBooks()}
            doneBooks={this.doneBooks()}
          />
        </div>
      </>
    );
  }
}

export default App;
