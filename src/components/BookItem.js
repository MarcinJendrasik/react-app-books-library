import React from "react";

const BookItem = props => {
  const style = {
    color: "red"
  };

  const {
    id,
    title,
    author,
    releaseDate,
    pages,
    link,
    date,
    important,
    active,
    finishDate
  } = props.user;

  if (active) {
    return (
      <div className="numbers">
        <span className="bolder">{id}</span>
        <img src={props.user.cover.small} alt="" />
        <h3 style={important ? style : null}>{title}</h3>
        <hr className="new" />
        <h4>
          <i>
            <span className="lighter">By {author}</span>
          </i>
        </h4>
        <p>
          <i>
            Release Date: {releaseDate}
            <br />
            Pages: {pages}
            <br />
            <span>
              Links: <a href={link}>shop</a>
            </span>
          </i>
          <br />
          <p>
            -do <span style={important ? style : null}>{date}</span>
          </p>
          <button onClick={() => props.change(id)}>Oddaj książkę</button>
        </p>
      </div>
    );
  } else {
    const finish = new Date(finishDate).toLocaleString();

    return (
      <div className="numbers">
        <span className="bolder">{id}</span>
        <img src={props.user.cover.small} alt="" />
        <h3>{title}</h3>
        <hr className="new" />
        <h4>
          <i>
            <span className="lighter">By {author}</span>
          </i>
        </h4>
        <p>
          <i>
            Release Date: {releaseDate}
            <br />
            Pages: {pages}
            <br />
            <span>
              Links: <a href={link}>shop</a>
            </span>
          </i>
          <br />
          <p>
            <em> (oddać do{date})</em>
            <br />
          </p>
        </p>
        Potwierdzenie oddania:
        <span> {finish} </span>
        <button className="confirm" onClick={() => props.delete(id)}>
          Zatwierdź
        </button>
      </div>
    );
  }
};
export default BookItem;
