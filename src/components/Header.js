import React from "react";
import "../styles/Header.css";

const Header = props => (
  <header>
    <div className="logo"></div>
    <h1>React'owe książki</h1>
    <hr className="style-three" />
  </header>
);

export default Header;
