import React from "react";
import "../styles/SectionBooks.css";
// import Book from "./Book.js";

const SectionBooks = props => {
  // const active = props.users.filter(user => user.active);
  // const done = props.users.filter(user => !user.active);
  // console.log(active, done);

  return (
    <>
      <section className="gallery">
        <div className="active">
          <h1>Książki wypożyczone:</h1>
          <section className="gallery">{props.activeBooks}</section>
          {/* <section className="gallery">{props.BookItem} </section> */}
        </div>
        <hr />
        <div className="done">
          <h1>Książki oddane:</h1>
          <section className="gallery">{props.doneBooks}</section>
        </div>
      </section>
    </>
  );
};

export default SectionBooks;
